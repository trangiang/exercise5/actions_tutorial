#include <ros/ros.h>
#include <std_srvs/Empty.h>

int main(int argc, char** argv)
{
    ros::init(argc, argv, "pantilt_follow_traj_client");
    ros::NodeHandle nh;

    // Create a client object for the spawn service. This
    // needs to know the data type of the service and its name.
    ros::ServiceClient setgoatClient = nh.serviceClient<std_srvs::Empty>("set_goal");
    ros::ServiceClient settolerancesClient = nh.serviceClient<std_srvs::Empty>("set_tolerances");

    // Create the request and response objects.
    std_srvs::Empty::Request req;
    std_srvs::Empty::Response resp;
    std_srvs::Empty::Request req1;
    std_srvs::Empty::Response resp1;

    ros::service::waitForService("set_goal", ros::Duration(5));
    bool success = setgoatClient.call(req, resp);
    ros::service::waitForService("set_tolerances", ros::Duration(5));
    bool success1 = setgoatClient.call(req1, resp1);

    if (success) {
        ROS_INFO_STREAM("Set a trajectory.");
    } else {
        ROS_ERROR_STREAM("Failed to set.");
    }

    if (success1) {
        ROS_INFO_STREAM("Set the tolerance parameters.");
    } else {
        ROS_ERROR_STREAM("Failed to set.");
    }
}